# run task schedule (with time-stamped output)
# min   hour    day     month   weekday command
*       *       *       *       *       artisan schedule:run | awk '{ print strftime("[%Y-%m-%d %H:%M:%S]"), $0; fflush(); }' > /dev/stdout

# do daily/weekly/monthly maintenance
# min   hour    day     month   weekday command
*/15    *       *       *       *       run-parts /etc/periodic/15min
0       *       *       *       *       run-parts /etc/periodic/hourly
0       2       *       *       *       run-parts /etc/periodic/daily
0       3       *       *       6       run-parts /etc/periodic/weekly
0       5       1       *       *       run-parts /etc/periodic/monthly