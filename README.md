# Intelligence Fusion API Test

## Assignment Requirements
Please fork this repository to begin.

Read [Getting Started](docs/getting_started.md) to create a working local environment then read the following documents to gain an understanding of our architecture and development approach:

- [Architecture](docs/architecture.md)
- [Principles](docs/principles.md)

The test assignment requirements can be found in [Requirements](docs/requirements.md). 

Once you have completed the task and are happy with your solution please forward a link to your forked repository with any supporting literature to [dev@intelligencefusion.co.uk](mailto:dev@intelligencefusion.co.uk)


