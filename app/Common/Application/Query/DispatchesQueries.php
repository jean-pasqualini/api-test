<?php

declare(strict_types=1);

namespace Fusion\Common\Application\Query;

use Prooph\Common\Messaging\Query;
use Prooph\ServiceBus\QueryBus;

trait DispatchesQueries
{
    /** @var QueryBus */
    protected $queryBus;

    /**
     * Dispatch a query and return the result
     *
     * @param Query $query
     *
     * @return mixed
     * @throws \Exception
     */
    protected function dispatchQuery(Query $query)
    {
        // Define what to do on success
        $successHandler = function ($data) use (&$result) {
            $result = $data;
        };

        // Define what to do on failure
        $failureHandler = function ($error) use (&$result) {
            $result = $error instanceof \Exception ? $error : new \RuntimeException($error);
        };

        // Send off the query, specifying the outcome handlers
        $this->queryBus()->dispatch($query)->then($successHandler, $failureHandler);

        // If the failure handler was used, throw the error or exception
        if ($result instanceof \Exception) {
            throw $result;
        }

        return $result;
    }

    /**
     * @return QueryBus
     */
    protected function queryBus(): QueryBus
    {
        if (!$this->queryBus instanceof QueryBus) {
            $this->queryBus = app(QueryBus::class);
        }

        return $this->queryBus;
    }
}
