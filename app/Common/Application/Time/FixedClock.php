<?php

namespace Fusion\Common\Application\Time;

class FixedClock implements Clock
{
    /**
     * @var \DateTimeImmutable
     */
    private $currentDatetime;

    public function __construct(\DateTimeImmutable $currentDatetime = null)
    {
        $this->currentDatetime = $currentDatetime ?? new \DateTimeImmutable();
    }

    public function now(): \DateTimeImmutable
    {
        return $this->currentDatetime;
    }
}
