<?php

declare(strict_types=1);

namespace Fusion\Common\Application\Command;

use Fusion\Common\Application\ApiUser;
use Fusion\Common\Application\Query\DispatchesQueries;
use Illuminate\Contracts\Auth\Factory as AuthManager;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\Plugin\Router\EventRouter;

/**
 * Command Handler
 *
 * @package Fusion\Common\Application\Command
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 * @version 1.0.0
 */
abstract class CommandHandler
{
    use DispatchesQueries;

    /** @var EventBus */
    protected $eventBus;
    /** @var EventRouter */
    protected $eventRouter;
    /** @var AuthManager */
    protected $authManager;

    // Setup ----

    /**
     * CommandHandler constructor.
     *
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus    = $eventBus;
        $this->eventRouter = new EventRouter();
        $this->authManager = app('auth'); #TODO: Inject this in parameters

        $this->eventRouter->attachToMessageBus($eventBus);
    }

    /**
     * @inheritDoc
     */
    public function __destruct()
    {
        $this->eventRouter->detachFromMessageBus($this->eventBus);
    }

    // Internals ----

    /**
     * Get the current user
     *
     * @return ApiUser|null
     */
    protected function currentUser(): ?ApiUser
    {
        return $this->authManager->guard()->user();
    }

    /**
     * Register one or more domain event listeners
     *
     * @param string     $eventName
     * @param callable[] $eventListeners
     */
    protected function listenForEvent(string $eventName, callable ...$eventListeners): void
    {
        foreach ($eventListeners as $eventListener) {
            $this->eventRouter->route($eventName)->to($eventListener);
        }
    }

    /**
     * Alias of listenForEvent
     *
     * @see CommandHandler::listenForEvent()
     *
     * @param string     $eventName
     * @param callable[] $eventListeners
     */
    protected function when(string $eventName, callable ...$eventListeners): void
    {
        $this->listenForEvent($eventName, ...$eventListeners);
    }
}
