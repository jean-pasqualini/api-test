<?php

namespace Fusion\Incidents\Application\Command;

use Fusion\Incidents\Domain\Entity\Incident;
use Prooph\ServiceBus\EventBus;

class CreateIncidentHandler
{
    /**
     * @var EventBus
     */
    private $eventBus;

    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function __invoke(CreateIncidentCommand $command) {
        Incident::create($command->getIncidentId(), $command->getData())->save();;
    }
}
