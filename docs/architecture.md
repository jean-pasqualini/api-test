# Architecture

The following is a brief description of the architecture of our API:

### Framework
Our API is built using Lumen and using PHP version 7.3. Our approach is to utilise core functionality of the framework without tying ourselves into the framework.

### REST API
We provide a REST API that is consumed by our ever growing list of customers. We adhere  to the REST guidelines providing a consistent API with correct response status codes used.

### Event Sourcing / CQRS
Our API is architected using the event sourcing paradigm utilising the command query response segregation pattern. This version of the API is for testing purposes only so does not include event sourced code however it does use commands and handlers for data writing and queries and handlers for data reading to provide an example of patterns we use.

### Domain Driven Development
Our application code has been developed using a domain driven architecture. Domain concepts are partitioned into their own domain (i.e. Incidents and Common within this test codebase) and within each domain we break our code into specific layers. 

- Presentation directory for code that sits on the outer most boundary such as HTTP controllers and console commands. 

- Application directory for utility/data handling classes such as command/query objects and handlers. 

- Infrastructure directory for service based classes.

- Domain directory for our core business logic such as repositories and entities.

### Repository Pattern
Lumen is known to actively promote the active record pattern however we have adopted the repository pattern within our API. We use the data mapper pattern for our entity objects and value objects for our sub classes. You will find examples within this codebase:

- Incident - entity object
- Position - value object.

### Storage Engines
We use Postgres as our main persistence service and Redis as our caching layer. As this repository is for testing purposes only this API uses Postgres only as the storage engine.
